﻿using System;

namespace KrakenClient
{
    public interface INonceGenerator
    {
        long GenerateNonce();
    }
    
    public class TimestampNonceGenerator : INonceGenerator
    {
        public long GenerateNonce()
        {
            // generate a 64 bit nonce using a timestamp at tick resolution
            return DateTime.Now.Ticks;
        }
    }
}