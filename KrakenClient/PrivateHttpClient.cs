﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace KrakenClient
{
    public class PrivateHttpClient
    {
        private readonly HttpClient _client;
        private readonly INonceGenerator _nonceGenerator;
        private readonly RequestSigner _signer;
        
        private readonly Uri _baseUri;
        private readonly string _key;
        private readonly int _version;
        
        public PrivateHttpClient(HttpClient client, INonceGenerator nonceGenerator, ApiConfiguration configuration)
        {
            _client = client;
            _nonceGenerator = nonceGenerator;
            _signer = new RequestSigner(configuration.Secret);
            
            _baseUri = new Uri(configuration.BaseAddress);
            _key = configuration.Key;
            _version = configuration.Version;
        }

        public Task<JObject> Post(string method, string props)
        {
            var path = GetPathFor(method);
            return DoPost(path, props);
        }
        
        private string GetPathFor(string method)
        {
            return $"/{_version}/private/{method}";
        }
        
        private async Task<JObject> DoPost(string path, string props)
        {
            var nonce = _nonceGenerator.GenerateNonce();
            props = PrepareRequest(props, nonce);

            var uri = new Uri(_baseUri, path);
            var signature = _signer.GenerateSignature(nonce, props, path);

            var content = CreateRequestContent(props, signature);
            return await MakeRequest(uri, content);
        }

        private static string PrepareRequest(string request, long nonce)
        {
            var nonceString = $"nonce={nonce}";
            return string.IsNullOrEmpty(request) ? nonceString : $"{nonceString}&{request}";
        }
        
        private async Task<JObject> MakeRequest(Uri uri, HttpContent content)
        {
            var response = await _client.PostAsync(uri, content);

            HandleInternalServerError(response);

            var contentString = await response.Content.ReadAsStringAsync();

            return JObject.Parse(contentString);
        }

        private StringContent CreateRequestContent(string props, string signature)
        {
            var content = new StringContent(props, Encoding.UTF8, "application/x-www-form-urlencoded");
            content.Headers.Add("API-Key", _key);
            content.Headers.Add("API-Sign", signature);
            return content;
        }

        private static void HandleInternalServerError(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                response.EnsureSuccessStatusCode();
            }
        }
    }
}