﻿namespace KrakenClient
{
    public class ApiConfiguration
    {
        public string BaseAddress { get; set; }
        public int Version { get; set; }
        public string Key { get; set; }
        public string Secret { get; set; }
    }
}