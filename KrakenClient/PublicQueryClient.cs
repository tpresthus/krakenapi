﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using KrakenClient.Models;
using Newtonsoft.Json.Linq;

namespace KrakenClient
{
    internal class PublicQueryClient
    {
        private readonly string _url;
        private readonly int _version;

        public PublicQueryClient(ApiConfiguration configuration)
        {
            _url = configuration.BaseAddress;
            _version = configuration.Version;
        }

        /// <summary>
        /// Get public server time. This is to aid in approximatig the skew time between the server and client
        /// </summary>
        public ServerTimeResponse GetServerTime()
        {
            return QueryPublic("Time").ToObject<ServerTimeResponse>();
        }

        /// <summary>
        /// Get a public list of active assets and their properties
        /// </summary>
        public ActiveAssetsResponse GetActiveAssets()
        {
            return QueryPublic("Assets").ToObject<ActiveAssetsResponse>();
        }

        /// <summary>
        /// Get a public list of tradable asset pairs
        /// </summary>
        /// <param name="pairs">Asset pairs to get information for.</param>
        /// <returns>List of tradable asset pairs</returns>
        public AssetPairsResponse GetAssetPairs(params string[] pairs)
        {
            var pairString = "";

            if (pairs.Any())
            {
                pairString = "pair=" + string.Join(",", pairs);
            }

            return QueryPublic("AssetPairs", pairString).ToObject<AssetPairsResponse>();
        }

        /// <summary>
        /// Get public ticker info for specific asset pairs.
        /// </summary>
        /// <param name="pairs">Asset pairs to get ticker information for.</param>
        public TickerResponse GetTicker(params string[] pairs)
        {
            if (!pairs.Any())
            {
                throw new ArgumentException("At least one asset pair must be supplied", nameof(pairs));
            }

            var pairString = "pair=" + string.Join(",", pairs);

            return QueryPublic("Ticker", pairString).ToObject<TickerResponse>();
        }

        /// <summary>
        /// Get public OHLC data for specific asset pairs.
        /// </summary>
        /// <param name="pair">Asset pair to get OHLC data for.</param>
        /// <param name="interval">Time frame interval in minutes:
        ///     1 (default), 5, 15, 30, 60, 240, 1440, 10080, 21600</param>
        /// <param name="since">Return committed OHLC data since given id (exclusive)</param>
        public OhlcDataResponse GetOhlcData(string pair, int interval, long since = 0)
        {
            if (string.IsNullOrEmpty(pair))
            {
                throw new ArgumentException("Asset pair must be supplied", nameof(pair));
            }

            var queryString = $"pair={pair}&interval={interval}";

            if (since > 0)
            {
                queryString = queryString + $"&since={since}";
            }

            var json = QueryPublic("OHLC", queryString);

            var response = new OhlcDataResponse
            {
                Error = json["error"].ToObject<List<string>>(),
                Result = OhlcData.Parse(json["result"].ToObject<JObject>())
            };

            return response;
        }

        /// <summary>
        /// Get public order book
        /// </summary>
        /// <param name="pair">Asset pair to get market depth for</param>
        public OrderBookResponse GetOrderBook(string pair)
        {
            if (string.IsNullOrEmpty(pair))
            {
                throw new ArgumentNullException(nameof(pair), "Pair must be specified");
            }

            var reqs = $"pair={pair}";

            return QueryPublic("Depth", reqs).ToObject<OrderBookResponse>();
        }

        /// <summary>
        /// Get public order book
        /// </summary>
        /// <param name="pair">Asset pair to get market depth for</param>
        /// <param name="count">Maximum number of ask/bids</param>
        public OrderBookResponse GetOrderBook(string pair, int count)
        {
            if (string.IsNullOrEmpty(pair))
            {
                throw new ArgumentNullException(nameof(pair), "Pair must be specified");
            }

            var reqs = $"pair={pair}&count={count}";

            return QueryPublic("Depth", reqs).ToObject<OrderBookResponse>();
        }

        /// <summary>
        /// Get recent trades
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        public RecentTradesResponse GetRecentTrades(string pair)
        {
            if (string.IsNullOrEmpty(pair))
            {
                throw new ArgumentNullException(nameof(pair), "Pair must be specified");
            }

            var reqs = $"pair={pair}";

            return QueryPublic("Trades", reqs).ToObject<RecentTradesResponse>();
        }

        /// <summary>
        /// Get recent trades
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        /// <param name="since">Return trade data since given id (exclusive)</param>
        public RecentTradesResponse GetRecentTrades(string pair, long since)
        {
            if (string.IsNullOrEmpty(pair))
            {
                throw new ArgumentNullException(nameof(pair), "Pair must be specified");
            }

            var reqs = $"pair={pair}&since={since}";

            return QueryPublic("Trades", reqs).ToObject<RecentTradesResponse>();
        }

        /// <summary>
        /// Get recent spread data
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        public RecentSpreadDataResponse GetRecentSpreadData(string pair)
        {
            if (string.IsNullOrEmpty(pair))
            {
                throw new ArgumentNullException(nameof(pair), "Pair must be specified");
            }

            var reqs = $"pair={pair}";

            return QueryPublic("Spread", reqs).ToObject<RecentSpreadDataResponse>();
        }

        /// <summary>
        /// Get recent spread data
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        /// <param name="since">Return trade data since given id (inclusive). Unix Time</param>
        public RecentSpreadDataResponse GetRecentSpreadData(string pair, long since)
        {
            if (string.IsNullOrEmpty(pair))
            {
                throw new ArgumentNullException(nameof(pair), "Pair must be specified");
            }

            var reqs = $"pair={pair}&since={since}";

            return QueryPublic("Spread", reqs).ToObject<RecentSpreadDataResponse>();
        }

        private JObject QueryPublic(string method, string props=null)
        {
            var address = $"{_url}/{_version}/public/{method}";
            var webRequest = WebRequest.Create(address);

            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";

            if (props != null)
            {
                using (var writer = new StreamWriter(webRequest.GetRequestStream()))
                {
                    writer.Write(props);
                }
            }

            try
            {
                //Wait for RateGate
                //_rateGate.WaitToProceed();

                using (var webResponse = webRequest.GetResponse())
                {
                    using (var str = webResponse.GetResponseStream())
                    {
                        using (var sr = new StreamReader(str))
                        {
                            return JObject.Parse(sr.ReadToEnd());
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                using (var response = (HttpWebResponse)wex.Response)
                {
                    using (var str = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(str))
                        {
                            if (response.StatusCode != HttpStatusCode.InternalServerError)
                            {
                                throw;
                            }
                            return JObject.Parse(sr.ReadToEnd());
                        }
                    }
                }

            }
        }
    }
}
