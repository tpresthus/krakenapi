﻿using Newtonsoft.Json.Linq;

namespace KrakenClient.Models
{
    public class TradeBalanceResponse : Response<TradeBalance>
    {
    }

    /// <summary>
    /// eb = equivalent balance (combined balance of all currencies)
    /// tb = trade balance (combined balance of all equity currencies)
    /// m = margin amount of open positions
    /// n = unrealized net profit/loss of open positions
    /// c = cost basis of open positions
    /// v = current floating valuation of open positions
    /// e = equity = trade balance + unrealized net profit/loss
    /// mf = free margin = equity - initial margin (maximum margin available to open new positions)
    /// ml = margin level = (equity / initial margin) * 100
    /// 
    /// Note: Rates used for the floating valuation is the midpoint of the best bid and ask prices
    /// </summary>
    public class TradeBalance
    {
        public double Eb { get; set; }
        public double Tb { get; set; }
        public double M { get; set; }
        public double N { get; set; }
        public double C { get; set; }
        public double V { get; set; }
        public double E { get; set; }
        public double Mf { get; set; }
    }
}