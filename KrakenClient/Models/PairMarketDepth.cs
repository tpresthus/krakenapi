﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace KrakenClient.Models
{
    public class OrderBookResponse : Response<Dictionary<string, PairMarketDepth>>
    {
    }
    
    /**
     * <pair_name> = pair name
       asks = ask side array of array entries(<price>, <volume>, <timestamp>)
       bids = bid side array of array entries(<price>, <volume>, <timestamp>)
     */
    public class PairMarketDepth
    {
        public JArray Asks { get; set; }
        public JArray Bids { get; set; }
    }
}