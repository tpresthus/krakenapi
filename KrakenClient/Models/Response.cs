﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace KrakenClient.Models
{
    public class Response<T>
    {
        public List<string> Error { get; set; }
        public T Result { get; set; }
    }
}