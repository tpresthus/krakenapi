﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace KrakenClient.Models
{

    public class OhlcDataResponse : Response<OhlcData>
    {
    }

    public class OhlcData
    {
        /// <summary>
        /// Key = Asset pair name
        /// Value = array of array entries(&lt;time&gt;, &lt;open&gt;, &lt;high&gt;, &lt;low&gt;, &lt;close&gt;, &lt;vwap&gt;, &lt;volume&gt;, &lt;count&gt;)
        /// </summary>
        public Dictionary<string, IReadOnlyCollection<Entry>> Assets { get; }

        /// <summary>
        /// Id to be used as since when polling for new, committed OHLC data
        /// </summary>
        public long Last { get; private set; }

        private OhlcData()
        {
            Assets = new Dictionary<string, IReadOnlyCollection<Entry>>();
        }

        internal static OhlcData Parse(JObject response)
        {
            var data = new OhlcData();

            foreach (var child in response)
            {
                if ("last".Equals(child.Key, StringComparison.InvariantCultureIgnoreCase))
                {
                    data.Last = child.Value.ToObject<long>();
                    continue;
                }

                data.Assets[child.Key] = child.Value.ToObject<JArray>()
                    .Select(j => j.ToObject<JArray>())
                    .Select(Entry.Parse)
                    .ToList();
            }

            return data;
        }

        public class Entry
        {
            public long Time { get; private set; }
            public decimal Open { get; private set; }
            public decimal High { get; private set; }
            public decimal Low { get; private set; }
            public decimal Close { get; private set; }
            public decimal VolumeWeightedAveragePrice { get; private set; }
            public decimal Volume { get; private set; }
            public long Count { get; private set; }

            private Entry()
            {
            }

            internal static Entry Parse(JArray json)
            {
                return new Entry
                {
                    Time = json[0].ToObject<long>(),
                    Open = json[1].ToObject<decimal>(),
                    High = json[2].ToObject<decimal>(),
                    Low = json[3].ToObject<decimal>(),
                    Close = json[4].ToObject<decimal>(),
                    VolumeWeightedAveragePrice = json[5].ToObject<decimal>(),
                    Volume = json[6].ToObject<decimal>(),
                    Count = json[7].ToObject<long>()
                };
            }
        }
    }
}
