﻿using System.Collections.Generic;

namespace KrakenClient.Models
{
    public class ActiveAssetsResponse : Response<Dictionary<string, Asset>>
    {
    }

    public class Asset
    {
        public string Aclass { get; set; }
        public string Altname { get; set; }
        public int Decimals { get; set; }
        public int Display_decimals { get; set; }
    }
}