﻿namespace KrakenClient.Models
{
    public class ServerTimeResponse : Response<ServerTime>
    {
    }
    
    public class ServerTime
    {
        public int Unixtime { get; set; }
        public string Rfc1123 { get; set; }
    }
}