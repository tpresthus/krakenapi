﻿using Newtonsoft.Json.Linq;

namespace KrakenClient.Models
{
    /**
     * TODO: This one's really tricky, because the Kraken API is poorly designed.
     * Basically, we get a response that looks like this:
     * {
         "error": [],
         "result": {
           "BCHEUR": [
             [
               "700000.000000",
               "0.00050000",
               1501603433.7669,
               "s",
               "l",
               ""
             ]
           ],
           "last": "1501605300157840478"
         }
       }
       
       Since the asset pair isn't separated from the "last" property, we will have
       to parse the json without having Json.NET deserialize it directly to types for
       us.
     */
    /**
     * <pair_name> = pair name
         array of array entries(<time>, <bid>, <ask>)
       last = id to be used as since when polling for new spread data
     */
    public class RecentSpreadDataResponse : Response<JObject>
    {
        
    }
}