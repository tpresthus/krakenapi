﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace KrakenClient.Models
{
    public class AssetPairsResponse : Response<Dictionary<string, AssetPair>>
    {
    }
    
    public class AssetPair
    {
        public string Altname { get; set; }
        public string Aclass_base { get; set; }
        public string Base { get; set; }
        public string Aclass_quote { get; set; }
        public string Quote { get; set; }
        public string Lot { get; set; }
        public int Pair_decimals { get; set; }
        public int Lot_decimals { get; set; }
        public int Lot_multiplier { get; set; }
        public JArray Leverage_buy { get; set; }
        public JArray Leverage_sell { get; set; }
        public List<List<decimal>> Fees { get; set; } // TODO: Create a better type for this. Response: "fees": [ [0, 0.26], [50000, 0.24] ]
        public List<List<decimal>> Fees_maker { get; set; }
        public string Fee_volume_currency { get; set; }
        public int Margin_call { get; set; }
        public int Margin_stop { get; set; }
    }
}