﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace KrakenClient.Models
{
    public class TickerResponse : Response<Dictionary<string, PairTicker>>
    {
    }
    
    /**
     * <pair_name> = pair name
       a = ask array(<price>, <whole lot volume>, <lot volume>),
       b = bid array(<price>, <whole lot volume>, <lot volume>),
       c = last trade closed array(<price>, <lot volume>),
       v = volume array(<today>, <last 24 hours>),
       p = volume weighted average price array(<today>, <last 24 hours>),
       t = number of trades array(<today>, <last 24 hours>),
       l = low array(<today>, <last 24 hours>),
       h = high array(<today>, <last 24 hours>),
       o = today's opening price
     */
    public class PairTicker
    {
        public JArray A { get; set; }
        public JArray B { get; set; }
        public JArray C { get; set; }
        public JArray V { get; set; }
        public JArray P { get; set; }
        public JArray T { get; set; }
        public JArray L { get; set; }
        public JArray H { get; set; }
        public decimal O { get; set; }
    }
}