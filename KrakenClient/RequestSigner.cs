﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace KrakenClient
{
    public class RequestSigner
    {
        private readonly string _secret;

        public RequestSigner(string secret)
        {
            _secret = secret;
        }

        public string GenerateSignature(long nonce, string props, string path)
        {
            var base64DecodedSecred = Convert.FromBase64String(_secret);

            var np = nonce + Convert.ToChar(0) + props;

            var pathBytes = Encoding.UTF8.GetBytes(path);
            var hash256Bytes = Sha256Hash(np);
            var z = new byte[pathBytes.Length + hash256Bytes.Length];
            pathBytes.CopyTo(z, 0);
            hash256Bytes.CopyTo(z, pathBytes.Length);

            var signature = GetMessageHash(base64DecodedSecred, z);
            return Convert.ToBase64String(signature);
        }
        
        private static byte[] Sha256Hash(string value)
        {
            using (var hash = SHA256.Create())
            {
                var enc = Encoding.UTF8;

                return hash.ComputeHash(enc.GetBytes(value));
            }
        }
        
        private static byte[] GetMessageHash(byte[] keyByte, byte[] messageBytes)
        {
            using (var hmacsha512 = new HMACSHA512(keyByte))
            {
                return hmacsha512.ComputeHash(messageBytes);
            }
        }
    }
}