﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using KrakenClient.Models;
using Newtonsoft.Json.Linq;

namespace KrakenClient
{
    public class KrakenClient
    {
        private readonly PublicQueryClient _publicClient;
        private readonly PrivateHttpClient _privateHttpClient;

        public KrakenClient(ApiConfiguration configuration) : this(new HttpClient(), configuration)
        {
        }

        public KrakenClient(HttpClient httpClient, ApiConfiguration configuration)
        {
            _publicClient = new PublicQueryClient(configuration);

            _privateHttpClient = new PrivateHttpClient(httpClient, new TimestampNonceGenerator(), configuration);
        }

        /// <summary>
        /// Get public server time. This is to aid in approximatig the skew time between the server and client
        /// </summary>
        public ServerTimeResponse GetServerTime()
        {
            return _publicClient.GetServerTime();
        }

        /// <summary>
        /// Get a public list of active assets and their properties
        /// </summary>
        public ActiveAssetsResponse GetActiveAssets()
        {
            return _publicClient.GetActiveAssets();
        }

        /// <summary>
        /// Get a public list of tradable asset pairs
        /// </summary>
        /// <param name="pairs">Asset pairs to get information for.</param>
        /// <returns>List of tradable asset pairs</returns>
        public AssetPairsResponse GetAssetPairs(params string[] pairs)
        {
            return _publicClient.GetAssetPairs(pairs);
        }

        /// <summary>
        /// Get public ticker info for specific asset pairs.
        /// </summary>
        /// <param name="pairs">Asset pairs to get ticker information for.</param>
        public TickerResponse GetTicker(params string[] pairs)
        {
            return _publicClient.GetTicker(pairs);
        }

        /// <summary>
        /// Get public OHLC data for specific asset pair.
        /// </summary>
        /// <param name="pair">Asset pair to get OHLC data for.</param>
        /// <param name="interval">Time frame interval in minutes:
        ///     1 (default), 5, 15, 30, 60, 240, 1440, 10080, 21600</param>
        /// <param name="since">Return committed OHLC data since given id (exclusive, optional)</param>
        public OhlcDataResponse GetOhlcData(string pair, int interval = 1, long since = 0)
        {
            return _publicClient.GetOhlcData(pair, interval, since);
        }

        /// <summary>
        /// Get public order book
        /// </summary>
        /// <param name="pair">Asset pair to get market depth for</param>
        public OrderBookResponse GetOrderBook(string pair)
        {
            return _publicClient.GetOrderBook(pair);
        }

        /// <summary>
        /// Get public order book
        /// </summary>
        /// <param name="pair">Asset pair to get market depth for</param>
        /// <param name="count">Maximum number of ask/bids</param>
        public OrderBookResponse GetOrderBook(string pair, int count)
        {
            return _publicClient.GetOrderBook(pair, count);
        }

        /// <summary>
        /// Get recent trades
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        public RecentTradesResponse GetRecentTrades(string pair)
        {
            return _publicClient.GetRecentTrades(pair);
        }

        /// <summary>
        /// Get recent trades
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        /// <param name="since">Return trade data since given id (exclusive)</param>
        public RecentTradesResponse GetRecentTrades(string pair, long since)
        {
            return _publicClient.GetRecentTrades(pair, since);
        }

        /// <summary>
        /// Get recent spread data
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        public RecentSpreadDataResponse GetRecentSpreadData(string pair)
        {
            return _publicClient.GetRecentSpreadData(pair);
        }

        /// <summary>
        /// Get recent spread data
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for</param>
        /// <param name="since">Return trade data since given id (inclusive). Unix Time</param>
        public RecentSpreadDataResponse GetRecentSpreadData(string pair, long since)
        {
            return _publicClient.GetRecentSpreadData(pair, since);
        }

        #region Private user data queries

        /// <summary>
        /// Get balance of users accounts.
        /// </summary>
        public async Task<BalanceResponse> GetBalance()
        {
            var response = await _privateHttpClient.Post("Balance", null);
            return response.ToObject<BalanceResponse>();
        }

        /// <summary>
        /// Get trade balance of users accounts.
        /// </summary>
        /// <param name="aclass">Asset class (optional): currency (default)</param>
        /// <param name="asset">Base asset used to determine balance (default = ZUSD)</param>
        public async Task<TradeBalanceResponse> GetTradeBalance(string aclass = "currency", string asset = "ZUSD")
        {
            var reqs = $"&aclass={aclass}&asset={asset}";

            var response = await _privateHttpClient.Post("TradeBalance", reqs);
            return response.ToObject<TradeBalanceResponse>();
        }

        /// <param name="trades">Whether or not to include trades in output (optional.  default = false)</param>
        /// <param name="userref">Restrict results to given user reference id (optional)</param>
        public async Task<OpenOrdersResponse> GetOpenOrders(bool trades = false, string userref = null)
        {
            var reqs = $"&trades={trades}";

            if (!string.IsNullOrEmpty(userref))
                reqs += $"&userref={userref}";

            var response = await _privateHttpClient.Post("OpenOrders", reqs);
            return response.ToObject<OpenOrdersResponse>();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="trades">whether or not to include trades in output (optional.  default = false)</param>
        /// <param name="userref">restrict results to given user reference id (optional)</param>
        /// <param name="start">starting unix timestamp or order tx id of results (optional.  exclusive)></param>
        /// <param name="end">ending unix timestamp or order tx id of results (optional.  inclusive)</param>
        /// <param name="ofs">result offset</param>
        /// <param name="closetime"> which time to use (optional) [open close both] (default)</param>
        /// <returns></returns>
        ///  /// <remarks>Note: Times given by order tx ids are more accurate than unix timestamps. If an order tx id is given for the time, the order's open time is used</remarks>
        public async Task<JObject> GetClosedOrders(bool trades=false, string userref="",string start="",string end="",string ofs="", string closetime="both")
        {
            var reqs = string.Format("&trades={0}&closetime={1}", trades,closetime);
            if (!string.IsNullOrEmpty(userref))
                reqs += string.Format("&useref={0}", userref);
            if (!string.IsNullOrEmpty(start))
                reqs += string.Format("&start={0}", start);
            if (!string.IsNullOrEmpty(end))
                reqs += string.Format("&end={0}", end);
            if (!string.IsNullOrEmpty(ofs))
                reqs += string.Format("&ofs={0}", ofs);

            var response = await _privateHttpClient.Post("ClosedOrders", reqs);
            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="txid">comma delimited list of transaction ids to query info about (20 maximum)</param>
        /// <param name="trades">whether or not to include trades in output (optional.  default = false)</param>
        /// <param name="userref">restrict results to given user reference id (optional)</param>
        /// <returns></returns>
        public async Task<JObject> QueryOrders(string txid, bool trades = false, string userref = "")
        {
            string reqs = string.Format("&txid={0}&trades={1}", txid, trades);
            if (!string.IsNullOrEmpty(userref))
                reqs += string.Format("&userref={0}", userref);

            var response = await _privateHttpClient.Post("QueryOrders", reqs);
            return response;
        }

       /// <summary>
       ///
       /// </summary>
        /// <param name="ofs">result offset</param>
        /// <param name="type">type of trade (optional) [all = all types (default), any position = any position (open or closed), closed position = positions that have been closed, closing position = any trade closing all or part of a position, no position = non-positional trades]</param>
        /// <param name="trades">whether or not to include trades related to position in output (optional.  default = false)</param>
        /// <param name="start">starting unix timestamp or trade tx id of results (optional.  exclusive)</param>
        /// <param name="end">ending unix timestamp or trade tx id of results (optional.  inclusive)</param>
       /// <returns></returns>
        public async Task<JObject> GetTradesHistory(string ofs="", string type="all", bool trades = false, string start = "", string end="")
        {
            string reqs = string.Format("&ofs={0}&type={1}&trades={2}", ofs, type, trades);
            if (!string.IsNullOrEmpty(start))
                reqs += string.Format("&start={0}", start);
            if (!string.IsNullOrEmpty(end))
                reqs += string.Format("&end={0}", end);

            var response = await _privateHttpClient.Post("TradesHistory", reqs);
            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="txid">comma delimited list of transaction ids to query info about (20 maximum)</param>
        /// <param name="trades">whether or not to include trades related to position in output (optional.  default = false)</param>
        /// <returns></returns>
        public async Task<JObject> QueryTrades(string txid="",bool trades=false)
        {
            string reqs = string.Format("&txid={0}&trades={1}", txid, trades);

            var response = await _privateHttpClient.Post("QueryTrades", reqs);
            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="txid">comma delimited list of transaction ids to query info about (20 maximum)</param>
        /// <param name="docalcs">whether or not to include profit/loss calculations (optional.  default = false)</param>
        /// <returns>
        /// position_txid = open position info
        /// ordertxid = order responsible for execution of trade
        /// pair = asset pair
        /// time = unix timestamp of trade
        /// type = type of order used to open position (buy/sell)
        /// ordertype = order type used to open position
        /// cost = opening cost of position (quote currency unless viqc set in oflags)
        /// fee = opening fee of position (quote currency)
        /// vol = position volume (base currency unless viqc set in oflags)
        /// vol_closed = position volume closed (base currency unless viqc set in oflags)
        /// margin = initial margin (quote currency)
        /// value = current value of remaining position (if docalcs requested.  quote currency)
        /// net = unrealized profit/loss of remaining position (if docalcs requested.  quote currency, quote currency scale)
        /// misc = comma delimited list of miscellaneous info
        /// oflags = comma delimited list of order flags
        /// viqc = volume in quote currency
        /// </returns>
        public async Task<JObject> GetOpenPositions(string txid = "", bool docalcs = false)
        {
            var reqs = string.Format("&txid={0}&docalcs={1}", txid, docalcs);

            var response = await _privateHttpClient.Post("OpenPositions", reqs);
            return response;
        }

       /// <summary>
       ///
       /// </summary>
       /// <param name="aclass">asset class (optional): currency (default)</param>
       /// <param name="asset">comma delimited list of assets to restrict output to (optional.  default = all) </param>
       /// <param name="type">type of ledger to retrieve (optional):[all(default) deposit withdrawal trade margin]</param>
       /// <param name="start">starting unix timestamp or ledger id of results (optional.  exclusive)</param>
       /// <param name="end">ending unix timestamp or ledger id of results (optional.  inclusive)</param>
       /// <param name="ofs">result offset</param>
       /// <returns>
       /// ledger_id = ledger info
       ///refid = reference id
       ///time = unx timestamp of ledger
       /// type = type of ledger entry
       ///aclass = asset class
       ///asset = asset
       ///amount = transaction amount
       ///fee = transaction fee
       ///balance = resulting balance
       /// </returns>
        public async Task<JObject> GetLedgers(string aclass = "currency", string asset = "all", string type = "all", string start = "", string end = "", string ofs = "")
        {
            var reqs = string.Format("&ofs={0}", ofs);
            if (!string.IsNullOrEmpty(aclass))
                reqs += string.Format("&aclass={0}", asset);
            if (!string.IsNullOrEmpty(type))
                reqs += string.Format("&type={0}", type);
            if (!string.IsNullOrEmpty(start))
                reqs += string.Format("&start={0}", start);
            if (!string.IsNullOrEmpty(end))
                reqs += string.Format("&end={0}", end);

            var response = await _privateHttpClient.Post("Ledgers", reqs);
            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id">comma delimited list of ledger ids to query info about (20 maximum)</param>
        /// <returns>ledger_id = ledger info.  See Get ledgers info</returns>
        public async Task<JObject> QueryLedgers(string id = "")
        {
            var reqs = string.Format("&id={0}", id);

            var response = await _privateHttpClient.Post("QueryLedgers", reqs);
            return response;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="pair">comma delimited list of asset pairs to get fee info on (optional)</param>
        /// <returns>currency = volume currency
        ///volume = current discount volume
        ///fees = array of asset pairs and fee tier info (if requested)
        ///fee = current fee in percent
        ///minfee = minimum fee for pair (if not fixed fee)
        ///maxfee = maximum fee for pair (if not fixed fee)
        ///nextfee = next tier's fee for pair (if not fixed fee.  nil if at lowest fee tier)
        ///nextvolume = volume level of next tier (if not fixed fee.  nil if at lowest fee tier)
        ///tiervolume = volume level of current tier (if not fixed fee.  nil if at lowest fee tier)
        ///</returns>
        public async Task<JObject> GetTradeVolume(string pair = "")
        {
            var reqs = string.Format("&pair={0}", pair);

            var response = await _privateHttpClient.Post("TradeVolume", reqs);
            return response;
        }

        #endregion

        #region Private user trading

        /// <summary>
        ///
        /// </summary>
        /// <param name="pair">asset pair</param>
        /// <param name="type">type of order (buy/sell)</param>
        /// <param name="ordertype">ordertype = order type:
        ///market
        ///limit (price = limit price)
        ///stop-loss (price = stop loss price)
        ///take-profit (price = take profit price)
        ///stop-loss-profit (price = stop loss price, price2 = take profit price)
        ///stop-loss-profit-limit (price = stop loss price, price2 = take profit price)
        ///stop-loss-limit (price = stop loss trigger price, price2 = triggered limit price)
        ///take-profit-limit (price = take profit trigger price, price2 = triggered limit price)
        ///trailing-stop (price = trailing stop offset)
        ///trailing-stop-limit (price = trailing stop offset, price2 = triggered limit offset)
        ///stop-loss-and-limit (price = stop loss price, price2 = limit price)</param>
        /// <param name="volume">order volume in lots</param>
        /// <param name="price">price (optional.  dependent upon ordertype)</param>
        /// <param name="price2">secondary price (optional.  dependent upon ordertype)</param>
        /// <param name="leverage">amount of leverage desired (optional.  default = none)</param>
        /// <param name="position">position tx id to close (optional.  used to close positions)</param>
        /// <param name="oflags">oflags = comma delimited list of order flags (optional):
        ///viqc = volume in quote currency
        ///plbc = prefer profit/loss in base currency
        ///nompp = no market price protection</param>
        /// <param name="starttm">scheduled start time (optional):
        ///0 = now (default)
        ///+n = schedule start time n seconds from now
        ///n = unix timestamp of start time</param>
        /// <param name="expiretm">expiration time (optional):
        /// 0 = no expiration (default)
        ///+n = expire n seconds from now
        ///n = unix timestamp of expiration time</param>
        /// <param name="userref">user reference id.  32-bit signed number.  (optional)</param>
        /// <param name="validate">validate inputs only.  do not submit order (optional)</param>
        /// <param name="close">optional closing order to add to system when order gets filled:
        ///close[ordertype] = order type
        ///close[price] = price
        ///close[price2] = secondary price</param>
        /// <returns>
        /// descr = order description info
        ///order = order description
        ///close = conditional close order description (if conditional close set)
        ///txid = array of transaction ids for order (if order was added successfully)
        /// </returns>
        public async Task<JObject> AddOrder(string pair,
            string type,
            string ordertype,
            decimal volume,
            decimal? price,
            decimal? price2,
            string leverage = "none",
            string position = "",
            string oflags = "",
            string starttm = "",
            string expiretm = "",
            string userref = "",
            bool validate = false,
            Dictionary<string, string> close = null)
        {
            var reqs = string.Format("&pair={0}&type={1}&ordertype={2}&volume={3}&leverage={4}", pair, type, ordertype, volume,leverage);
            if (price.HasValue)
                reqs += string.Format("&price={0}", price.Value);
            if (price2.HasValue)
                reqs += string.Format("&price2={0}", price2.Value);
            if (!string.IsNullOrEmpty(position))
                reqs += string.Format("&position={0}", position);
            if (!string.IsNullOrEmpty(starttm))
                reqs += string.Format("&starttm={0}", starttm);
            if (!string.IsNullOrEmpty(expiretm))
                reqs += string.Format("&expiretm={0}", expiretm);
            if (!string.IsNullOrEmpty(oflags))
                reqs += string.Format("&oflags={0}", oflags);
            if (!string.IsNullOrEmpty(userref))
                reqs += string.Format("&userref={0}", userref);
            if (validate)
                reqs += "&validate=true";
            if (close != null)
            {
                var closeString = string.Format("&close[ordertype]={0}&close[price]={1}&close[price2]={2}",close["ordertype"],close["price"],close["price2"]);
                reqs += closeString;
            }

            var response = await _privateHttpClient.Post("AddOrder", reqs);
            return response;
        }

        public Task<JObject> AddOrder(KrakenOrder krakenOrder)
        {
            return AddOrder(pair : krakenOrder.Pair,
                            type : krakenOrder.Type,
                            ordertype : krakenOrder.OrderType,
                            volume : krakenOrder.Volume,
                            price : krakenOrder.Price,
                            price2 : krakenOrder.Price2,
                            leverage : krakenOrder.Leverage??"none",
                            position : krakenOrder.Position??string.Empty,
                            oflags : krakenOrder.OFlags??string.Empty,
                            starttm: krakenOrder.Starttm ?? string.Empty,
                            expiretm: krakenOrder.Expiretm ?? string.Empty,
                            userref: krakenOrder.Userref ?? string.Empty,
                            validate : krakenOrder.Validate,
                            close : krakenOrder.Close);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="txid">transaction id</param>
        /// <returns>
        /// count = number of orders canceled
        ///pending = if set, order(s) is/are pending cancellation
        /// </returns>
        public async Task<JObject> CancelOrder(string txid)
        {
            var reqs = string.Format("&txid={0}", txid);

            var response = await _privateHttpClient.Post("CancelOrder", reqs);
            return response;
        }

        #endregion
    }
}
