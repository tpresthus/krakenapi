# C# Kraken API for .NET Core

Originally a fork of https://bitbucket.org/arrivets/krakenapi that implemented a basic example of how to integrate with the Kraken API from C# on .NET full framework.

The original implementation that this version is forked from handles rate-limiting (1 request/5s) using Jack Leitch's RateGate.

This fork is a port to .NET standard 2.0 making it available to applications targeting .NET core as well as those targeting the full framework. Since the rate-limiting library isn't available for netstandard2.0, it has been temporary dropped.

The solution includes two projects:

- KrakenClient.csproj which exposes the API methods found here: https://www.kraken.com/help/api  
- KrakenClient.Tests.csproj which provides verification tests of the client implementation.

## Migration notes

Since netstandard2.0 doesn't provide the `System.Configuration.ConfigurationManager`, the `KrakenClient` now accepts an `ApiConfiguration` object instead of relying on appsettings. 
