using System;
using System.Net.Http;
using System.Text;

namespace KrakenClient.Tests
{
    public static class ClientHelper
    {
        public static KrakenClient CreateClient(HttpMessageHandler messageHandler)
        {
            var httpClient = new HttpClient(messageHandler);
            return CreateClient(httpClient, DefaultConfiguration());
        }
        
        public static KrakenClient CreateClient(Action<ApiConfiguration> options = null)
        {
            var config = DefaultConfiguration();
            options?.Invoke(config);

            return CreateClient(new HttpClient(), config);
        }
        
        private static KrakenClient CreateClient(HttpClient httpClient, ApiConfiguration config)
        {
            return new KrakenClient(httpClient, config);
        }

        private static ApiConfiguration DefaultConfiguration()
        {
            return new ApiConfiguration
            {
                BaseAddress = "https://api.kraken.com",
                Version = 0,
                Key = Convert.ToBase64String(Encoding.UTF8.GetBytes("key")),
                Secret = Convert.ToBase64String(Encoding.UTF8.GetBytes("secret"))
            };
        }
    }
}