﻿using Xunit;

namespace KrakenClient.Tests.Private
{
    public class GetBalance
    {
        [Fact]
        public async void CanGetBalance()
        {
            var client = ClientHelper.CreateClient(new FakeHttpMessageHandler()
                .WithJsonResponse(
                    "https://api.kraken.com/0/private/Balance", 
                    "{\"error\": []," + 
                    " \"result\": {" + 
                    "   \"XLTC\": \"0.0510768000\"," +
                    "   \"XRP\": \"10.11\"" +
                    "}}"));

            var balance = await client.GetBalance();
            
            Assert.Equal(0.0510768, balance.Result["XLTC"]);
            Assert.Equal(10.11, balance.Result["XRP"]);
        }
    }
}