﻿using Xunit;

namespace KrakenClient.Tests.Private
{
    public class GetTradeBalance
    {
        [Fact]
        public async void CanGetTradeBalance()
        {
            var client = ClientHelper.CreateClient(new FakeHttpMessageHandler()
                .WithJsonResponse(
                    "https://api.kraken.com/0/private/TradeBalance", 
                    "{\"error\": []," + 
                    " \"result\": {" + 
                    "   \"eb\": \"10.5979\"," +
                    "   \"tb\": \"0.0000\"," +
                    "   \"m\": \"1.0000\"," +
                    "   \"n\": \"2.0000\"," +
                    "   \"c\": \"3.0000\"," +
                    "   \"v\": \"4.0000\"," +
                    "   \"e\": \"5.0000\"," +
                    "   \"mf\": \"6.0000\"" +
                    "}}")
            );

            var tradeBalance = await client.GetTradeBalance();
            
            Assert.Equal(10.5979, tradeBalance.Result.Eb);
            Assert.Equal(0, tradeBalance.Result.Tb);
            Assert.Equal(1, tradeBalance.Result.M);
            Assert.Equal(2, tradeBalance.Result.N);
            Assert.Equal(3, tradeBalance.Result.C);
            Assert.Equal(4, tradeBalance.Result.V);
            Assert.Equal(5, tradeBalance.Result.E);
            Assert.Equal(6, tradeBalance.Result.Mf);
        }
    }
}