﻿using System.Linq;
using Xunit;

namespace KrakenClient.Tests.Private
{
    public class GetOpenOrders
    {
        [Fact]
        public async void CanGetOpenOrders()
        {
            var client = ClientHelper.CreateClient(new FakeHttpMessageHandler()
                .WithJsonResponse(
                    "https://api.kraken.com/0/private/OpenOrders", 
                    "{\"error\": []," + 
                    " \"result\": {" + 
                    "   \"open\": { " + 
                    "     \"some-transaction-id\": { " + 
                    "       \"refid\": \"some-reference-id\"," +
                    "       \"userref\": \"some-user-ref\"," +
                    "       \"status\": \"open\"," +
                    "       \"opentm\": \"12345\"," +
                    "       \"starttm\": \"12346\"," +
                    "       \"expiretm\": \"12347\"," +
                    "       \"descr\": {}," +
                    "       \"vol\": \"6.0\"," +
                    "       \"vol_exec\": \"3.0\"," +
                    "       \"cost\": \"70.0\"," +
                    "       \"fee\": \"1.0\"," +
                    "       \"price\": \"30.5\"," +
                    "       \"stopprice\": \"31.0\"," +
                    "       \"limitprice\": \"30.7\"," +
                    "       \"misc\": \"\"," +
                    "       \"oflags\": \"\"," +
                    "       \"trades\": [\"some-trade-id\"]" +
                    "     } " + 
                    "   } " +
                    "}}")
            );

            var openOrders = await client.GetOpenOrders();

            Assert.NotNull(openOrders);
            var order = openOrders.Result.Open["some-transaction-id"];
            
            Assert.Equal("some-reference-id", order.RefId);
            Assert.Equal("some-user-ref", order.Userref);
            Assert.Equal("open", order.Status);
            Assert.Equal(12345, order.Opentm);
            Assert.Equal(12346, order.Starttm);
            Assert.Equal(12347, order.Expiretm);
            Assert.Equal(6.0, order.Vol);
            Assert.Equal(3.0, order.Vol_exec);
            Assert.Equal(70.0, order.Cost);
            Assert.Equal(1.0, order.Fee);
            Assert.Equal(30.5, order.Price);
            Assert.Equal(31.0, order.Stopprice);
            Assert.Equal(30.7, order.Limitprice);
            Assert.Equal("", order.Misc);
            Assert.Equal("", order.Oflags);
            Assert.Equal("some-trade-id", order.Trades.First());
        }
    }
}