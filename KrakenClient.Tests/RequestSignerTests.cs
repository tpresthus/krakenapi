﻿using System;
using System.Text;
using Xunit;

namespace KrakenClient.Tests
{
    public class RequestSignerTests
    {
        [Fact]
        public void GeneratesCorrectSignature()
        {
            const string expectedSignature = "BmVcqg3krR85zeqWvPZgfdEjlSxWiofwWy8y+MZrfoweirE6NxiDXepVozYewoH4oNp9n12VWfv+0aCMguCptQ==";
            
            var secret = CreateSecret("this-is-secret");
            var signer = new RequestSigner(secret);
            
            var signature = signer.GenerateSignature(10, "foo=bar&baz=boo", "/some/path");
            
            Assert.Equal(expectedSignature, signature);
        }

        private static string CreateSecret(string secret)
        {
            var bytes = Encoding.UTF8.GetBytes(secret);
            return Convert.ToBase64String(bytes);
        }
    }
}