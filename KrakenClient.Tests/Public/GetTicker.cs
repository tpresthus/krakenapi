using System;
using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetTicker
    {
        [Fact]
        public void AtLeastOnePairMustBeSupplied()
        {
            var client = ClientHelper.CreateClient();

            Assert.Throws<ArgumentException>("pairs", () =>
            {
                var assetPairs = client.GetTicker();
            });
        }
        
        [Fact]
        public void CanGetTickerForOnePair()
        {
            var client = ClientHelper.CreateClient();

            var ticker = client.GetTicker("BCHEUR");
            
            Assert.NotNull(ticker);
            Assert.Equal(1, ticker.Result.Count);
        }
        
        [Fact]
        public void CanGetTickerForMultiplePair()
        {
            var client = ClientHelper.CreateClient();

            var ticker = client.GetTicker("BCHEUR", "BCHUSD");
            
            Assert.NotNull(ticker);
            Assert.Equal(2, ticker.Result.Count);
        }
    }
}