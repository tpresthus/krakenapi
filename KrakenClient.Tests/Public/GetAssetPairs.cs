using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetAssetPairs
    {
        [Fact]
        public void CanGetAllAssetPairs()
        {
            var client = ClientHelper.CreateClient();

            var assetPairs = client.GetAssetPairs();
            
            Assert.NotNull(assetPairs);
            Assert.NotEmpty(assetPairs.Result);
        }
        
        [Fact]
        public void CanGetSpecificPair()
        {
            var client = ClientHelper.CreateClient();

            var assetPairs = client.GetAssetPairs("BCHEUR");
            
            Assert.NotNull(assetPairs);
            Assert.Equal(1, assetPairs.Result.Count);
            Assert.Equal("BCHEUR", assetPairs.Result["BCHEUR"].Altname);
        }
    }
}