using System;
using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetRecentTrades
    {
        [Fact]
        public void PairMustBeSupplied()
        {
            var client = ClientHelper.CreateClient();

            Assert.Throws<ArgumentNullException>("pair", () =>
            {
                var assetPairs = client.GetRecentTrades("");
            });
            
            Assert.Throws<ArgumentNullException>("pair", () =>
            {
                var assetPairs = client.GetRecentTrades(null);
            });
        }
        
        [Fact]
        public void CanGetTradesWithoutSince()
        {
            var client = ClientHelper.CreateClient();

            var trades = client.GetRecentTrades("BCHEUR");
            
            Assert.NotNull(trades);
        }
        
        [Fact]
        public void CanGetTradesWithSince()
        {
            var client = ClientHelper.CreateClient();

            var trades = client.GetRecentTrades("BCHEUR", 5000);
            
            Assert.NotNull(trades);
        }
    }
}