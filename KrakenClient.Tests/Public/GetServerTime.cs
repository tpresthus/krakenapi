using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetServerTime
    {
        [Fact]
        public void CanGetServerTime()
        {
            var client = ClientHelper.CreateClient();

            var serverTime = client.GetServerTime();
            
            Assert.True(serverTime.Result.Unixtime > 0);
            Assert.NotNull(serverTime.Result.Rfc1123);
        }
    }
}