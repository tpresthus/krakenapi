using System;
using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetOrderBook
    {
        [Fact]
        public void PairMustBeSupplied()
        {
            var client = ClientHelper.CreateClient();

            Assert.Throws<ArgumentNullException>("pair", () =>
            {
                var assetPairs = client.GetOrderBook("");
            });
            
            Assert.Throws<ArgumentNullException>("pair", () =>
            {
                var assetPairs = client.GetOrderBook(null);
            });
        }
        
        [Fact]
        public void CanGetOrderBookWithoutMaxNumber()
        {
            var client = ClientHelper.CreateClient();

            var orderBook = client.GetOrderBook("BCHEUR");
            
            Assert.NotNull(orderBook);
            Assert.Equal(1, orderBook.Result.Count);
        }
        
        [Fact]
        public void CanGetOrderBookWithMaxNumber()
        {
            var client = ClientHelper.CreateClient();

            var orderBook = client.GetOrderBook("BCHEUR", 1);
            
            Assert.Equal(1, orderBook.Result.Count);
            Assert.Equal(1, orderBook.Result["BCHEUR"].Asks.Count);
            Assert.Equal(1, orderBook.Result["BCHEUR"].Bids.Count);
        }
    }
}