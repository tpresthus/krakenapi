using System.Linq;
using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetActiveAssets
    {
        [Fact]
        public void CanGetActiveAssets()
        {
            var client = ClientHelper.CreateClient();

            var activeAssets = client.GetActiveAssets();
            
            Assert.NotNull(activeAssets);
            
            Assert.NotNull(activeAssets.Result.First().Key);
            Assert.NotNull(activeAssets.Result.First().Value.Aclass);
            Assert.NotNull(activeAssets.Result.First().Value.Altname);
        }
    }
}