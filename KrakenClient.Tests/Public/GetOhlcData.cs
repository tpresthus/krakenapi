using System;
using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetOhlcData
    {
        [Fact]
        public void PairCanNotBeNull()
        {
            var client = ClientHelper.CreateClient();

            Assert.Throws<ArgumentException>("pair", () =>
            {
                var ohlcData = client.GetOhlcData(null);
            });
        }

        [Fact]
        public void PairCanNotBeEmpty()
        {
            var client = ClientHelper.CreateClient();

            Assert.Throws<ArgumentException>("pair", () =>
            {
                var ohlcData = client.GetOhlcData(string.Empty);
            });
        }

        [Fact]
        public void CanGetOhlcData()
        {
            var client = ClientHelper.CreateClient();

            var ohlcData = client.GetOhlcData("BCHEUR");

            Assert.NotNull(ohlcData);
            Assert.NotEmpty(ohlcData.Result.Assets["BCHEUR"]);
            Assert.True(ohlcData.Result.Last > 0);
        }
    }
}
