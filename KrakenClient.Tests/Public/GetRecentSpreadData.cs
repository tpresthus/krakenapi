using System;
using Xunit;

namespace KrakenClient.Tests.Public
{
    public class GetRecentSpreadData
    {
        [Fact]
        public void PairMustBeSupplied()
        {
            var client = ClientHelper.CreateClient();

            Assert.Throws<ArgumentNullException>("pair", () =>
            {
                var assetPairs = client.GetRecentSpreadData("", 0);
            });
            
            Assert.Throws<ArgumentNullException>("pair", () =>
            {
                var assetPairs = client.GetRecentSpreadData(null, 0);
            });
        }
        
        [Fact]
        public void CanGetSpreadDataWithoutSince()
        {
            var client = ClientHelper.CreateClient();

            var spreadData = client.GetRecentSpreadData("BCHEUR");
            
            Assert.NotNull(spreadData);
        }
        
        [Fact]
        public void CanGetSpreadDataWithSince()
        {
            var client = ClientHelper.CreateClient();

            var spreadData = client.GetRecentSpreadData("BCHEUR", 5000);
            
            Assert.NotNull(spreadData);
        }
    }
}