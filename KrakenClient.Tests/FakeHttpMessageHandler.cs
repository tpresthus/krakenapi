﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KrakenClient.Tests
{
    public class FakeHttpMessageHandler : HttpMessageHandler
    {
        private readonly HttpMethod _expectedMethod = HttpMethod.Post;
        private const string ExpectedContentType = "application/x-www-form-urlencoded";

        private readonly Dictionary<string, string> _responses = new Dictionary<string, string>();

        public FakeHttpMessageHandler WithJsonResponse(string uri, string content)
        {
            _responses[uri] = content;
            return this;
        }
        
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Method != _expectedMethod)
            {
                throw new Exception($"Expected HTTP method {_expectedMethod} but got {request.Method}");
            }

            if (request.Content.Headers.ContentType.MediaType != ExpectedContentType)
            {
                throw new Exception($"Expected Content-Type {ExpectedContentType} but got {request.Content.Headers.ContentType.MediaType}");
            }

            var uri = request.RequestUri.ToString();
            if (!_responses.ContainsKey(uri))
            {
                throw new Exception($"Received request for unexpected URI: {uri}");
            }

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(_responses[uri], Encoding.UTF8, "application/json")
            };

            return Task.FromResult(response);
        }
    }
}